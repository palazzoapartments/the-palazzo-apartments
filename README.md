At The Palazzo Apartments in San Marcos, Texas, you can count on finding alternative design options including newly updated, roomy floor plans and stunning amenities available in an unbeatable location. Our San Marcos apartments offer a huge selection of lavish one, two, and three bedroom apartment homes to meet your individual needs.

Address: 1011 Wonder World Drive, San Marcos, TX 78666, USA

Phone: 866-427-5759

Website: https://www.thepalazzoliving.com
